package main

import (
	"errors"
	"fmt"

	"github.com/cucumber/godog"
)

func insertToTheDatabase(todoItem string) error {
	if len(todoItem) == 0 {
		return errors.New("Todo item cannot be empty")
	}
	return nil
}

func newTodoRequest(todoItem string) error {
	TodoItem = todoItem
	return nil
}

func returnHttpStatusCodeIs(httpStatus int) error {
	if len(TodoItem) == 0 && httpStatus != 400 {
		return fmt.Errorf("When todo is empty should return http 400, but is %d", httpStatus)
	}

	if len(TodoItem) > 0 && httpStatus != 200 {
		return fmt.Errorf("When todo is not empty should return 200, but is %d", httpStatus)
	}
	return nil
}

func InitializeScenario(ctx *godog.ScenarioContext) {
	ctx.Step(`^new todo request "([^"]*)"$`, newTodoRequest)
	ctx.Step(`^insert "([^"]*)" to the database$`, insertToTheDatabase)
	ctx.Step(`^return http status code is (\d+)$`, returnHttpStatusCodeIs)
}
