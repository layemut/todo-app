Feature: New todo item can be added to list

  Scenario: New todo item with valid string
    Given new todo request "Buy Cheese"
    Then insert "Buy Cheese" to the database
    And return http status code is 200

  Scenario: New todo item with empty string
    Given new todo request ""
    Then return http status code is 400
