package todo

import "errors"

type Service struct {
	DB *map[int]string
}

func New() *Service {
	return &Service{
		DB: &map[int]string{},
	}
}

func (ts *Service) Insert(todoItem string) error {
	if len(todoItem) == 0 {
		return errors.New("todo item cannot be empty")
	}

	size := len(*ts.DB)

	(*ts.DB)[size+1] = todoItem

	return nil
}

func (ts *Service) ListAll() (todoList []string) {
	for _, v := range *ts.DB {
		todoList = append(todoList, v)
	}
	return
}
