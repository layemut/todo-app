package todo

import (
	"errors"
	"testing"
)

func TestInsert(t *testing.T) {
	todoService := New()

	t.Run("Insert todo item", func(t *testing.T) {
		todoItem := "Buy Cheese"
		var want error
		got := todoService.Insert(todoItem)
		if got != want {
			t.Errorf("Inserting todo should return %v error, but got %v", want, got)
		}
	})

	t.Run("Insert empty todo item", func(t *testing.T) {
		todoItem := ""
		want := errors.New("todo item cannot be empty")
		got := todoService.Insert(todoItem)
		if got.Error() != want.Error() {
			t.Errorf("Inserting empty todo shoudl return %v, but got %v", want, got)
		}
	})
}
