module todo-api

go 1.17

require (
	github.com/cucumber/godog v0.12.4
	github.com/go-chi/chi v1.5.4
)

require (
	github.com/cucumber/gherkin-go/v19 v19.0.3 // indirect
	github.com/cucumber/messages-go/v16 v16.0.1 // indirect
	github.com/go-chi/cors v1.2.0
	github.com/gofrs/uuid v4.0.0+incompatible // indirect
	github.com/hashicorp/go-immutable-radix v1.3.0 // indirect
	github.com/hashicorp/go-memdb v1.3.0 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
